﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IMSS
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form medic = new medicos();
            medic.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form enfermo = new pacientes();
            enfermo.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form citas = new consultas();
            citas.Show();
        }
    }
}
