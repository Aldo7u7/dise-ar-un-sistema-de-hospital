﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IMSS
{
    public partial class medicos : Form
    {
        DataAccess.DataSet1TableAdapters.medicosTableAdapter tamedicos = new DataAccess.DataSet1TableAdapters.medicosTableAdapter();
        public medicos()
        {
            InitializeComponent();
        }

        private void medicos_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSet1.medicos' table. You can move, or remove it, as needed.
            this.medicosTableAdapter.Fill(this.dataSet1.medicos);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            tamedicos.Insert(textBox2.Text);
            int.Parse(textBox1.Text);
            tamedicos.Insert(textBox1.Text);
            this.medicosTableAdapter.Fill(this.dataSet1.medicos);

        }

        private void button2_Click(object sender, EventArgs e)
        {
            tamedicos.DeleteQuery((int)comboBox1.SelectedValue);
            this.medicosTableAdapter.Fill(this.dataSet1.medicos);
        }
    }
}
