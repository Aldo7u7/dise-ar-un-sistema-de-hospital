﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IMSS
{
    public partial class pacientes : Form
    {
        DataAccess.DataSet1TableAdapters.pacientesTableAdapter tapacientes = new DataAccess.DataSet1TableAdapters.pacientesTableAdapter();
        public pacientes()
        {
            InitializeComponent();
        }

        private void pacientes_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSet1.pacientes' table. You can move, or remove it, as needed.
            this.pacientesTableAdapter.Fill(this.dataSet1.pacientes);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            tapacientes.Insert(textBox2.Text);
            int.Parse(textBox1.Text);
            tapacientes.Insert(textBox1.Text);
            this.pacientesTableAdapter.Fill(this.dataSet1.pacientes);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            tapacientes.DeleteQuery((int)comboBox1.SelectedValue);
            this.pacientesTableAdapter.Fill(this.dataSet1.pacientes);
        }
    }
}
