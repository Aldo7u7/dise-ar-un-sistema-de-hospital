﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IMSS
{
    public partial class consultas : Form
    {
        DataAccess.DataSet1TableAdapters.consultasTableAdapter taconsultas = new DataAccess.DataSet1TableAdapters.consultasTableAdapter();
        public consultas()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            taconsultas.Insert(textBox2.Text);
            int.Parse(textBox1.Text);
            taconsultas.Insert(textBox1.Text);
            this.consultasTableAdapter.Fill(this.dataSet1.consultas);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            taconsultas.DeleteQuery((int)comboBox1.SelectedValue);
            this.consultasTableAdapter.Fill(this.dataSet1.consultas);
        }

        private void consultas_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSet1.consultas' table. You can move, or remove it, as needed.
            this.consultasTableAdapter.Fill(this.dataSet1.consultas);

        }
    }
}
